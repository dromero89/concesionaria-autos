var angularApp = angular.module('lpApp.services', []);

angularApp.factory('serviceInfo', function ($http) {

    return $http.get('/public/jsons/main.json').then(function (response) {
        return response.data;
    });

});
angularApp.factory('dataService', function ($http) {

    return {
        oportunidades: function () {
            return $http.get('/public/jsons/oportunidades.json')
                .then(function (response) {
                    return response.data;
                });
        },
        planes: function () {
            return $http.get('/public/jsons/planes.json')
                .then(function (response) {
                    return response.data;
                });

        }
    }
});
angularApp.factory('serviceSurcusales', function ($http) {

    return $http.get('/public/jsons/sucursales.json')
        .then(function (response) {
            return response.data;
        });

});

angularApp.factory('FormService', function ($http, $q) {

    var FormService = {};

    FormService.sucursal = {};
    FormService.gridVehicle = {};

    FormService.createContact = function (contact) {
        return $http.post('/api/contacts', contact)
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                return error.data
            })
    };

    return FormService;
});