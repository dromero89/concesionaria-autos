'use strict';

var angularApp = angular.module('lpApp.controllers', []);

angularApp.controller('FormController', function ($scope, $timeout, serviceSurcusales, dataService, FormService) {
    var $ctrl = this;
    $ctrl.formDefinition = {};
    $ctrl.formDefinition.sucursales = [];
    $ctrl.formDefinition.vehicles = [];

    $ctrl.formModalDefinition = {};
    $ctrl.formModalDefinition.vehicles = [];

    $ctrl.formData = {};
    $ctrl.formData.branch = {};
    $ctrl.formData.vehicle = {};
    $ctrl.formData.gridVehicle = {};

    $ctrl.submitContactForm = function (fromModal, event) {
        event && event.preventDefault();
        var vehicleName = fromModal ? $ctrl.formData.gridVehicle.name : $ctrl.formData.vehicle.name;
        var isValid = fromModal ? $scope.contactModalForm.$valid : $scope.contactForm.$valid;
        var duration = 8000;
        if (isValid) {
            FormService.createContact({
                    name: $ctrl.formData.name,
                    lastName: $ctrl.formData.lastName,
                    phone: $ctrl.formData.phone,
                    email: $ctrl.formData.email,
                    branch: $ctrl.formData.branch.name,
                    vehicle: vehicleName,
                    sourcePage: $scope.currentPage
                })
                .then(function (response) {
                    if (response.type == 'success') {
                        console.log(response.data);
                        $ctrl.showSuccessMessage = true;
                        $timeout(function(){
                            $ctrl.showSuccessMessage = false;
                        }, duration);
                    }
                    if (response.type == 'error') {
                        console.log(response.data);
                        
                        $ctrl.showErrorMessage = true;
                        $timeout(function(){
                            $ctrl.showErrorMessage = false;
                        }, duration);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    };

    activate();

    function activate() {
        serviceSurcusales
            .then(function (sucursales) {
                $ctrl.formDefinition.sucursales = sucursales[$scope.currentPage];
                $ctrl.formData.branch = $ctrl.formDefinition.sucursales[0];
            })
            .catch(function (err) {
                console.log(err)
            });
        dataService[$scope.currentPage]()
            .then(function (vehiclesJSON) {
                var vehicles = vehiclesJSON.vehicles;
                var sliderVehicles = [];
                var gridVehicles = [];
                for (var i = 0; i < vehicles.length; i++) {
                    gridVehicles.push(vehicles[i]);
                    if (vehicles[i].imageslider.length > 0) {
                        sliderVehicles.push(vehicles[i]);
                    }
                }
                $ctrl.formModalDefinition.vehicles = gridVehicles;
                $ctrl.formDefinition.vehicles = sliderVehicles;
                //TODO: resolve which to show
                $ctrl.formData.vehicle = $ctrl.formDefinition.vehicles[0];
                $ctrl.formData.gridVehicle = $ctrl.formModalDefinition.vehicles[0];
            })
            .then(function () {
                for (var i = 0; i < $ctrl.formModalDefinition.vehicles.length; i++) {
                    var modalVehicle = $ctrl.formModalDefinition.vehicles[i];
                    if (modalVehicle.name === FormService.gridVehicle.name) {
                        $ctrl.formData.gridVehicle = modalVehicle;
                        break;
                    }
                }
            })
            .catch(function (err) {
                console.log(err)
            });

        $scope.$watch(function () {
            return FormService.vehicle;
        }, function (oldVehicle, newVehicle) {
            if (oldVehicle !== newVehicle) {
                var selectVehicle = {};
                for (var i = 0; i < $ctrl.formDefinition.vehicles.length; i++) {
                    var vehicle = $ctrl.formDefinition.vehicles[i];
                    if (vehicle.name == FormService.vehicle.name) {
                        selectVehicle = vehicle;
                        break;
                    }
                }
                $ctrl.formData.vehicle = vehicle;
            }
        });

    }

});

angularApp.controller('OportunidadesCtrl', function ($scope, $filter, $log, $uibModal, serviceInfo, dataService) {
    $scope.active = 0;
    var car, i;
    dataService[$scope.currentPage]()
        .then(function (vehiclesJSON) {
            $scope.carsCollection = {Todos: []};
            for (i = 0; i < vehiclesJSON.vehicles.length; i++) {
                car = vehiclesJSON.vehicles[i];
                $scope.carsCollection[car.vehicle_type] = $scope.carsCollection[car.vehicle_type] || [];

                $scope.carsCollection[car.vehicle_type].push(car);
                $scope.carsCollection.Todos.push(car);

            }

            angular.forEach($scope.carsCollection, function (value, key) {
                $scope.carsCollection[key] = $filter('orderBy')(value, 'price', false, function (v1, v2) {
                    if (v1.type === 'string' && v2.type === 'string') {
                        v1.value = parseFloat(v1.value);
                        v2.value = parseFloat(v2.value);

                        return (v1.value < v2.value) ? -1 : 1;
                    }
                });
            });

            //set order, so we can place featured car by url as order = 0, to show up first
            for (var j = 0; j < $scope.carsCollection.length; j++) {
                car = $scope.carsCollection[j];
                car.order = i + 1;
            }

            if ($scope.vehicleUrlName) {
                var firstGridCarIndex = 0;
                for (i = 0; i < $scope.carsCollection['Todos'].length; i++) {
                    car = $scope.carsCollection['Todos'][i];
                    if (car.url_name === $scope.vehicleUrlName) {
                        firstGridCarIndex = i;
                        break;
                    }
                }
                console.log($scope.carsCollection['Todos'][firstGridCarIndex]);
                $scope.carsCollection['Todos'][firstGridCarIndex].order = 0;
            }

        })
        .catch(function (err) {
            console.log(err)
        });

    $scope.openVehicleModal = function (vehicle) {
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            windowClass: 'modal-wide',
            templateUrl: 'vehicle-detail-modal.html',
            controller: function ($rootScope, $uibModalInstance, vehicle, FormService) {
                var $ctrl = this;
                $ctrl.vehicle = vehicle;

                $ctrl.ok = function () {
                    $uibModalInstance.close();
                };

                $ctrl.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                $rootScope.$broadcast('ModalForm.vehicle', {
                    vehicle: vehicle
                });
                FormService.gridVehicle = vehicle;
            },
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                vehicle: function () {
                    return vehicle;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $ctrl.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
});

angularApp.controller('sliderController', function ($scope, $filter, dataService, FormService) {
    var $ctrl = this;
    $ctrl.sliderCollection = [];
    dataService[$scope.currentPage]()
        .then(function (vehiclesJSON) {
            var cars = vehiclesJSON.vehicles;
            for (var i = 0; i < cars.length; i++) {
                var car = cars[i];
                if (car.imageslider.length > 0) {
                    $ctrl.sliderCollection.push(car);
                }
            }

            $ctrl.sliderCollection = $filter('orderBy')($ctrl.sliderCollection, 'price', false, function (v1, v2) {
                if (v1.type === 'string' && v2.type === 'string') {
                    v1.value = parseFloat(v1.value);
                    v2.value = parseFloat(v2.value);

                    return (v1.value < v2.value) ? -1 : 1;
                }
            });

        })
        .then(function () {
            var initSlideIndex = getInitSlideIndex();

            $ctrl.slickConfig = {
                enabled: true,
                autoplay: false,
                draggable: true,
                autoplaySpeed: 2000,
                arrows: true,
                adaptiveHeight: true,
                event: {
                    afterChange: function (event, slick, currentSlide, nextSlide) {
                        FormService.vehicle = $ctrl.sliderCollection[currentSlide];
                    },
                    init: function (event, slick) {
                        slick.slickGoTo(initSlideIndex);
                    }
                }
            };

            function getInitSlideIndex() {
                var initSlideIndex = 0;
                for (var i = 0; i < $ctrl.sliderCollection.length; i++) {
                    var slide = $ctrl.sliderCollection[i];
                    if (slide.url_name == $scope.vehicleUrlName) {
                        initSlideIndex = i;
                    }
                }
                return initSlideIndex;
            }
        })
        .catch(function (err) {
            console.log(err)
        });
});

angularApp.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
