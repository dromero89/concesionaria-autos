const express = require('express');
const expressHbs = require('express-handlebars');
const path = require('path');
const helpers = require('handlebars-helpers')();
const Promise = require('bluebird');
const mongoose = require('mongoose');
const compression = require('compression');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

require('dotenv').config();

//suppress promise warning
mongoose.Promise = Promise;

const app = express();
const hbs = expressHbs.create({
    extname: 'hbs',
    defaultLayout: 'main.hbs',
    helpers: Object.assign(helpers, {
        'raw-helper': function (options) {
            return options.fn();
        }
    })
});


app.engine('hbs', hbs.engine);

app.use('/public', express.static('public'));

app.set('port', (process.env.PORT || 5050));

app.set('view engine', 'hbs');

app.use(compression());

app.use(cookieParser());

app.use(bodyParser.json({
    limit: '50mb'
}));

app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));

require('./routes/routes')(app);

const Server = function () {
};

Server.start = function (port = app.get('port')) {
    connect()
        .on('error', console.log)
        .on('disconnected', connect)
        .once('open', listen);

    function listen(){
        app.listen(port, function () {
            console.log('magic happens on port', port)
        });
    }

    function connect () {
        var options = { server: { socketOptions: { keepAlive: 1 } } };
        return mongoose.connect(process.env.MONGO_URL, options).connection;
    }
};

module.exports = Server;