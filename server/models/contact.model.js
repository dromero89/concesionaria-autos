const mongoose = require('mongoose');

const ContactSchema = new mongoose.Schema({
    name: {type: String, required: [true, 'Nombre y Apellido son obligatorios.'], default: '', trim: true},
    lastName: {type: String, required: [true, 'Nombre y Apellido son obligatorios.'], default: '', trim: true},
    phone: {type: String, required: [true, 'Teléfono es obligatorio.'], default: '', trim: true},
    email: {type: String, required: [true, 'Email es obligatorio.'], default: '', trim: true},
    branch: {type: String, required: true, default: '', trim: true},
    vehicle: {type: String, required: true, default: '', trim: true},
    sourcePage: {type: String, required: true, default: '', trim: true},
    createdAt: {type: Date, default: Date.now},
    mkt: {type: String}
});

module.exports = mongoose.model('Contact', ContactSchema);