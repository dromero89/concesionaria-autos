const fs = require('fs');
const path = require('path');
const hbs = require('handlebars');

module.exports = {
    loadHbsPartial: function loadHbsPartial(relativePathFromPartialsDir) {
        function loadPartialString(relativePathFromPartialsDir) {
            return fs.readFileSync(path.join(__dirname, '../..', `views/partials/${relativePathFromPartialsDir}`), 'utf8')
        }

        return hbs.compile(loadPartialString(relativePathFromPartialsDir));
    }
};