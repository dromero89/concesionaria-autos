const Promise = require('bluebird');
const helper = require('./helper');
const only = require('only');
const Contact = require('../models/contact.model');

module.exports = function (app) {

    app.use(function (req, res, next) {
        if (req.query.gclid || req.query.utm_campaign) {
            let options = {
                maxAge: 1000 * 60 * 60 * 24 * 180, // 180 days
                httpOnly: true, // The cookie only accessible by the web server
                signed: false
            };
            const URL = req.protocol + '://' + req.get('host') + req.originalUrl;
            res.cookie('mkt', URL, options);
        }

        next();
    });

    app.get('/oportunidades/:vehicle_url_name?', function (req, res) {
        return res.render('index', {
            currentPage: 'oportunidades',
            vehicle_url_name: req.params.vehicle_url_name,
            partials: Promise.resolve({
                banner: helper.loadHbsPartial('oportunidades/banner.hbs')
            })
        });

    });

    app.get('/planes/:vehicle_url_name?', function (req, res) {
        return res.render('planes', {
            currentPage: 'planes',
            vehicle_url_name: req.params.vehicle_url_name,
            partials: Promise.resolve({
                banner: helper.loadHbsPartial('planes/banner.hbs')
            })
        });
    });

    app.get('/:vehicle_url_name?', function (req, res) {
        return res.render('index', {
            currentPage: 'oportunidades',
            vehicle_url_name: req.params.vehicle_url_name,
            partials: Promise.resolve({
                banner: helper.loadHbsPartial('oportunidades/banner.hbs')
            })
        });
    });

    app.post('/api/contacts', function (req, res) {
        const contact = new Contact(only(req.body, 'name lastName phone email branch vehicle sourcePage'));
        if(req.cookies.mkt){
            contact.mkt = req.cookies.mkt;
        }
        console.log(contact);
        return contact.save()
            .then(contact => {
                if (!contact) {
                    return res.status(400).json({
                        type: 'error',
                        data: 'There was an error on the server, please try again later.'
                    })
                }
                return res.status(200).json({
                    type: 'success',
                    data: contact
                })
            })
            .catch(err => {
                return res.status(404).json({
                    type: 'error',
                    data: err
                })
            });
    });
};